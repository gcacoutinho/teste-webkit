# Repository de Teste/POC do Aplicativo
## Abaxio estão algumas fontes de referência

- [Docs WebView](https://developer.apple.com/documentation/webkit/webview)
- [Stack Overflow - Ponte App <=> WebView | Possivel solução (15/05/2017)](https://stackoverflow.com/questions/43916436/enable-camera-and-mic-access-in-wkwebview)
- [Stack Overflow - Incompatibilidade WebRTC - WebView (24/05/2018)](https://stackoverflow.com/questions/45055329/does-webkit-in-ios-11-beta-support-webrtc)
- [Stack Overflow - Pergunta sobre Scan de Código S/R (11/02/2019)](https://stackoverflow.com/questions/54630265/web-page-cannot-access-camera-in-webview)
- [Forum Apple - Pergunta sobre compatibilidade WebRTC - WebView (24/09/2017)](https://forums.developer.apple.com/thread/88052#thread-post-278803)

 Conclusões sobre o que foi visto nos links acima é de que por enquanto a Apple
não tem um suporte oficial ao "getUserMedia" do WebRTC no WebView e que, apesar
de talvez poder ser feito por meios alternativos, não é garantida a estabilidade.

 E como foi comentado no quarto link, o ideal seria fazer o Scan de forma nativa 
e passar o necessário para o JS da página.