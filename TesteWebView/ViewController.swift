//
//  ViewController.swift
//  TesteWebView
//
//  Created by Gabriel Coutinho on 29/08/19.
//  Copyright © 2019 Gabriel Coutinho. All rights reserved.
//

import UIKit
import WebKit

class ViewController: UIViewController {

    @IBOutlet var webView: WKWebView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        
        let url = URL(string: "https://serratus.github.io/quaggaJS/examples/live_w_locator.html")
        let requestObj = URLRequest(url: url! as URL)
        webView.load(requestObj)
    }
}

